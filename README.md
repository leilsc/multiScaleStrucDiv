# multiScaleStrucDiv

Git repository to accompany the manuscript 'An empirical Bayesian approach to quantify multi-scale spatial structural diversity in remote sensing data'. 

Version 0.1.1 of the R package StrucDiv can be downloaded from CRAN and contains methods to quantify scale-specific spatial structural diversity. Version 0.2.0 contains the multi-scale approach and is provided as in the file StrucDiv2_0.1.0.tar.gz, which is due to be submitted to CRAN. R scripts can be found in the methods folder. Execute the file dir_create.R, respective folders will be created and the results will be saved therein as .tif files, if R scripts in the methods folder are executed.