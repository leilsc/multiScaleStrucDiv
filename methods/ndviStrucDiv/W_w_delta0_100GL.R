#============================================================
# Quantify spatial structural diversity across scales
#============================================================
# Metric: structural diversity entropy, delta = 0
#============================================================
library(raster)
library(strucDiv)
#============================================================

vi100 <- raster('data/ndvi100GL.tif')
length(unique(vi100))

#============================================================
# wslI  = 3
#============================================================

ndvi_delta0_w3_nn <- strucDiv(vi100, wsl  = 3, dist = 1, angle = "all",
                       rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w3_nn, "results/ndviStrucDiv/ndvi_delta0_w3_not-nested_100GL.tif")

ndvi_delta0_w3_W7 <- strucDivNest(vi100, wslI  = 3, wslO = 7, dist = 1, angle = "all",
                                rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w3_W7, "results/ndviStrucDiv/ndvi_delta0_w3_W7_100GL.tif")

ndvi_delta0_w3_W13 <- strucDivNest(vi100, wslI  = 3, wslO = 13, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w3_W13, "results/ndviStrucDiv/ndvi_delta0_w3_W13_100GL.tif")

ndvi_delta0_w3_W19 <- strucDivNest(vi100, wslI  = 3, wslO = 19, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w3_W19, "results/ndviStrucDiv/ndvi_delta0_w3_W19_100GL.tif")

ndvi_delta0_w3_W35 <- strucDivNest(vi100, wslI  = 3, wslO = 35, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w3_W35, "results/ndviStrucDiv/ndvi_delta0_w3_W35_100GL.tif")

# ndvi_delta0_w3_B35 <- strucDivNest(vi100, wslI  = 3, dimB = c(35, 35), oLap = xxx, dist = 1, angle = "all",
#                                    rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)
# 
# writeRaster(ndvi_delta0_w3_W35, "results/ndviStrucDiv/ndvi_delta0_w3_W35_100GL.tif")

ndvi_delta0_w3_D <- strucDivNest(vi100, wslI  = 3, domain = TRUE, dist = 1, angle = "all",
                                   rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w3_D, "results/ndviStrucDiv/ndvi_delta0_w3_D_100GL.tif")

#============================================================
# wslI  = 7
#============================================================

ndvi_delta0_w7_nn <- strucDiv(vi100, wsl  = 7, dist = 1, angle = "all",
                       rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w7_nn, "results/ndviStrucDiv/ndvi_delta0_w7_not-nested_100GL.tif")

ndvi_delta0_w7_W13 <- strucDivNest(vi100, wslI = 7, wslO = 13, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w7_W13, "results/ndviStrucDiv/ndvi_delta0_w7_W13_100GL.tif")

ndvi_delta0_w7_W19 <- strucDivNest(vi100, wslI = 7, wslO = 19, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w7_W19, "results/ndviStrucDiv/ndvi_delta0_w7_W19_100GL.tif")

ndvi_delta0_w7_W35 <- strucDivNest(vi100, wslI = 7, wslO = 35, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w7_W35, "results/ndviStrucDiv/ndvi_delta0_w7_W35_100GL.tif")

ndvi_delta0_w7_D <- strucDivNest(vi100, wslI  = 7, domain = TRUE, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w7_D, "results/ndviStrucDiv/ndvi_delta0_w7_D_100GL.tif")

#============================================================
# wslI  = 13
#============================================================

ndvi_delta0_w13_nn <- strucDiv(vi100, wsl  = 13, dist = 1, angle = "all",
                       rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w13_nn, "results/ndviStrucDiv/ndvi_delta0_w13_not-nested_100GL.tif")

ndvi_delta0_w13_W19 <- strucDivNest(vi100, wslI = 13, wslO = 19, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w13_W19, "results/ndviStrucDiv/ndvi_delta0_w13_W19_100GL.tif")

ndvi_delta0_w13_W35 <- strucDivNest(vi100, wslI = 13, wslO = 35, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w13_W35, "results/ndviStrucDiv/ndvi_delta0_w13_W35_100GL.tif")

ndvi_delta0_w13_D <- strucDivNest(vi100, wslI  = 13, domain = TRUE, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 0, na.handling = na.pass)

writeRaster(ndvi_delta0_w13_D, "results/ndviStrucDiv/ndvi_delta0_w13_D_100GL.tif")

#============================================================



