#============================================================
# Quantify spatial structural diversity across scales
#============================================================
# Metric: structural diversity entropy, delta = 2
#============================================================
library(raster)
library(strucDiv)
#============================================================

vi10 <- raster('data/ndvi10GL.tif')
length(unique(vi10))

#============================================================
# wslI  = 3
#============================================================

ndvi_delta2_w3_nn <- strucDiv(vi10, wsl  = 3, dist = 1, angle = "all",
                       rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_nn, "results/ndviStrucDiv/ndvi_delta2_w3_not-nested_10GL.tif")

ndvi_delta2_w3_W7 <- strucDivNest(vi10, wslI  = 3, wslO = 7, dist = 1, angle = "all",
                                rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_W7, "results/ndviStrucDiv/ndvi_delta2_w3_W7_10GL.tif")

ndvi_delta2_w3_W13 <- strucDivNest(vi10, wslI  = 3, wslO = 13, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_W13, "results/ndviStrucDiv/ndvi_delta2_w3_W13_10GL.tif")

ndvi_delta2_w3_W19 <- strucDivNest(vi10, wslI  = 3, wslO = 19, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_W19, "results/ndviStrucDiv/ndvi_delta2_w3_W19_10GL.tif")

ndvi_delta2_w3_W35 <- strucDivNest(vi10, wslI  = 3, wslO = 35, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_W35, "results/ndviStrucDiv/ndvi_delta2_w3_W35_10GL.tif")

# ndvi_delta2_w3_B35 <- strucDivNest(vi10, wslI  = 3, dimB = c(35, 35), oLap = xxx, dist = 1, angle = "all",
#                                    rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)
# 
# writeRaster(ndvi_delta2_w3_W35, "results/ndviStrucDiv/ndvi_delta2_w3_W35_10GL.tif")

ndvi_delta2_w3_D <- strucDivNest(vi10, wslI  = 3, domain = TRUE, dist = 1, angle = "all",
                                   rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_D, "results/ndviStrucDiv/ndvi_delta2_w3_D_10GL.tif")

#============================================================
# wslI  = 7
#============================================================

ndvi_delta2_w7_nn <- strucDiv(vi10, wsl  = 7, dist = 1, angle = "all",
                       rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_nn, "results/ndviStrucDiv/ndvi_delta2_w7_not-nested_10GL.tif")

ndvi_delta2_w7_W13 <- strucDivNest(vi10, wslI = 7, wslO = 13, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_W13, "results/ndviStrucDiv/ndvi_delta2_w7_W13_10GL.tif")

ndvi_delta2_w7_W19 <- strucDivNest(vi10, wslI = 7, wslO = 19, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_W19, "results/ndviStrucDiv/ndvi_delta2_w7_W19_10GL.tif")

ndvi_delta2_w7_W35 <- strucDivNest(vi10, wslI = 7, wslO = 35, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_W35, "results/ndviStrucDiv/ndvi_delta2_w7_W35_10GL.tif")

ndvi_delta2_w7_D <- strucDivNest(vi10, wslI  = 7, domain = TRUE, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_D, "results/ndviStrucDiv/ndvi_delta2_w7_D_10GL.tif")

#============================================================
# wslI  = 13
#============================================================

ndvi_delta2_w13_nn <- strucDiv(vi10, wsl  = 13, dist = 1, angle = "all",
                       rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w13_nn, "results/ndviStrucDiv/ndvi_delta2_w13_not-nested_10GL.tif")

ndvi_delta2_w13_W19 <- strucDivNest(vi10, wslI = 13, wslO = 19, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w13_W19, "results/ndviStrucDiv/ndvi_delta2_w13_W19_10GL.tif")

ndvi_delta2_w13_W35 <- strucDivNest(vi10, wslI = 13, wslO = 35, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w13_W35, "results/ndviStrucDiv/ndvi_delta2_w13_W35_10GL.tif")

ndvi_delta2_w13_D <- strucDivNest(vi10, wslI  = 13, domain = TRUE, dist = 1, angle = "all",
                                 rank = FALSE, fun = entropy, delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w13_D, "results/ndviStrucDiv/ndvi_delta2_w13_D_10GL.tif")

#============================================================



