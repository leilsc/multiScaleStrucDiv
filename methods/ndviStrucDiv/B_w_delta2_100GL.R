#============================================================
# Quantify spatial structural diversity across scales
#============================================================
# Metric: structural diversity entropy, delta = 2
#============================================================
library(raster)
library(StrucDiv2)
#============================================================

vi100 <- raster('data/ndvi100GL.tif')

#============================================================
# wslI  = 3
#============================================================

ndvi_delta2_w3_B35_100GL <- strucDivNest(vi100, wslI  = 3, dimB = c(35,35), oLap = 10, priorB = TRUE, 
                              dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                              delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_B35_100GL, "results/ndviStrucDiv/ndvi_delta2_w3_B35_100GL.tif")

ndvi_delta2_w3_B54_100GL <- strucDivNest(vi100, wslI  = 3, dimB = c(54,54), oLap = 15, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_B54_100GL, "results/ndviStrucDiv/ndvi_delta2_w3_B54_100GL.tif")

ndvi_delta2_w3_B72_100GL <- strucDivNest(vi100, wslI  = 3, dimB = c(72,72), oLap = 19, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_B72_100GL, "results/ndviStrucDiv/ndvi_delta2_w3_B72_100GL.tif")

ndvi_delta2_w3_B84_100GL <- strucDivNest(vi100, wslI  = 3, dimB = c(84, 84), oLap = 22, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w3_B84_100GL, "results/ndviStrucDiv/ndvi_delta2_w3_B84_100GL.tif")

#============================================================
# wslI  = 7
#============================================================

ndvi_delta2_w7_B35_100GL <- strucDivNest(vi100, wslI = 7, dimB = c(35,35), oLap = 12, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_B35_100GL, "results/ndviStrucDiv/ndvi_delta2_w7_B35_100GL.tif")

ndvi_delta2_w7_B54_100GL <- strucDivNest(vi100, wslI = 7, dimB = c(54,54), oLap = 17, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_B54_100GL, "results/ndviStrucDiv/ndvi_delta2_w7_B54_100GL.tif")

ndvi_delta2_w7_B72_100GL <- strucDivNest(vi100, wslI = 7, dimB = c(72,72), oLap = 21, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_B72_100GL, "results/ndviStrucDiv/ndvi_delta2_w7_B72_100GL.tif")

ndvi_delta2_w7_B84_100GL <- strucDivNest(vi100, wslI = 7, dimB = c(84, 84), oLap = 24, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w7_B84_100GL, "results/ndviStrucDiv/ndvi_delta2_w7_B84_100GL.tif")

#============================================================
# wslI  = 13
#============================================================

ndvi_delta2_w13_B35_100GL <- strucDivNest(vi100, wslI = 13, dimB = c(35,35), oLap = 15, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w13_B35_100GL, "results/ndviStrucDiv/ndvi_delta2_w13_B35_100GL.tif")

ndvi_delta2_w13_B54_100GL <- strucDivNest(vi100, wslI = 13, dimB = c(54,54), oLap = 20, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w13_B54_100GL, "results/ndviStrucDiv/ndvi_delta2_w13_B54_100GL.tif")

ndvi_delta2_w13_B72_100GL <- strucDivNest(vi100, wslI = 13, dimB = c(72,72), oLap = 24, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w13_B72_100GL, "results/ndviStrucDiv/ndvi_delta2_w13_B72_100GL.tif")

ndvi_delta2_w13_B84_100GL <- strucDivNest(vi100, wslI = 13, dimB = c(84, 84), oLap = 27, priorB = TRUE, 
                                        dist = 1, angle = "all", rank = FALSE, fun = entropy, 
                                        delta = 2, na.handling = na.pass)

writeRaster(ndvi_delta2_w13_B84_100GL, "results/ndviStrucDiv/ndvi_delta2_w13_B84_100GL.tif")
#============================================================



